// spanish
import commonEs from 'public/locales/es/common.json';
const resources = {
  es: {
    common: commonEs,
  },
};

export default resources;
