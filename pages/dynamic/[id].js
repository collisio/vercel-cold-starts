import React from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { Typography } from '@mui/material';
const Page = ({ test }) => {
  const {t} = useTranslation('common')
  return (
  <div>
    <Typography>Hello!</Typography>
    <h1>{test}</h1>
    <h1>{t('example')}</h1>
  </div>
)};

export default Page;

export async function getServerSideProps({locale}) {
  return {
    props: {
      test: "Hi",
      ...await serverSideTranslations(locale, ['common']),
    },
  };
}

